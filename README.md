toolbox
========

My tool box to collect tool scripts etc., including usual ubuntu settings.


Fetch the Source
----------------

The default name `toolbox` matters when setting up the hooks and helper scripts of this tool box.

```
git clone <repo rul> toolbox
```


Shell Config
------------

You may activate the shell configuration by appending this line in your `~/.bash_aliases`

```
source $HOME/toolbox/config/bash/hook-all.sh
```
