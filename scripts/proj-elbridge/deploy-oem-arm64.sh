set -x

deploy_sut() {
    local sut=$1
    scp -r ./image-builds-rm-git.tar.gz $sut:~/
    ssh $sut sudo rm -rf /home/ubuntu/image-builds-rm-git
    ssh $sut tar -zxf /home/ubuntu/image-builds-rm-git.tar.gz
}

rm -rf image-builds-rm-git.tar.gz image-builds-rm-git

cp -r image-builds image-builds-rm-git
cd image-builds-rm-git
rm -rf .git

pushd ../
tar -zcf image-builds-rm-git.tar.gz image-builds-rm-git
# d05-4 - focal
deploy_sut "ubuntu@10.228.68.152"
# d05-5 - jammy
deploy_sut "ubuntu@10.228.68.190"
# papat - jammy
deploy_sut "ubuntu@10.229.1.94"
popd
