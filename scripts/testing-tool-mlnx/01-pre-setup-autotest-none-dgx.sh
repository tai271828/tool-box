#!/bin/sh

set -e
set -x

export DEBIAN_FRONTEND=noninteractive
NVIDIA_DRIVER_VERSION=450-server

install_mellanox_ofed() {
    local MLNX_REPO="https://linux.mellanox.com/public/repo/mlnx_ofed"
    local ubuntu_ver
    ubuntu_ver="$(lsb_release -rs)"
    case $ubuntu_ver in
	20.04)
	    MELLANOX_OFED_VERSION=5.1-2.6.2.0
	    ;;
	18.04)
	    MELLANOX_OFED_VERSION=4.9-2.2.4.0
	    ;;
	*)
	    echo "ERROR: Unknown Ubuntu Release" 1>&2
	    return 1
    esac
    mlnx_url="${MLNX_REPO}/${MELLANOX_OFED_VERSION}/ubuntu${ubuntu_ver}/mellanox_mlnx_ofed.list"
    
    wget -O - https://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox | \
	apt-key add -
    wget -O /etc/apt/sources.list.d/mellanox_mlnx_ofed.list \
	 "$mlnx_url"
    apt update
    apt install mlnx-ofed-kernel-only -y
}

install_fabric_manager() {
    apt install -y nvidia-fabricmanager-${NVIDIA_DRIVER_VERSION%-server}
    systemctl enable nvidia-fabricmanager
}

if [ "$(dmidecode -s baseboard-product-name)" = "DGXA100" ] &&
       [ "$(ls /sys/bus/node/devices | wc -l)" -ne 2 ]; then
    echo "ERROR: DGX A100 systems should be configured with NPS=1 for performance testing"
    echo "Please correct the setting in the BIOS Setup Menu:"
    echo "  Advanced->AMD CBS->DF Common Options->Advanced->Memory Addressing"
    exit 1
fi

pkgs="nvidia-utils-${NVIDIA_DRIVER_VERSION}"
pkgs="${pkgs} nvidia-kernel-source-${NVIDIA_DRIVER_VERSION}"
for metapkg in $(dpkg-query -W -f '${Package}\n' "linux-generic*"); do
    flavor=${metapkg#linux-}
    pkgs="$pkgs linux-modules-nvidia-${NVIDIA_DRIVER_VERSION}-${flavor}"
done
apt install -y ${pkgs}
#install_fabric_manager
install_mellanox_ofed
#update-initramfs -u

#apt remove --purge irqbalance -y
