#!/bin/bash

autotest_client_tests_repo="https://gitlab.com/tai271828/autotest-client-tests.git"
autotest_client_tests_branch="dev-init-ubuntu-mlnx"

cd ${HOME}

sudo apt-get install git python2 gdb python-yaml -y || sudo apt-get install git python gdb python-yaml -y
git clone --depth=1 ${autotest_client_tests_repo} -b ${autotest_client_tests_branch}
git clone --depth=1 git://kernel.ubuntu.com/ubuntu/autotest

rm -fr autotest/client/tests
ln -sf ~/autotest-client-tests autotest/client/tests

ln -s ~/toolbox/scripts/testing-tool-mlnx/03-run-debug.sh ~/
ln -s ~/toolbox/scripts/testing-tool-mlnx/03-run-debug-01-one-job.sh ~/

