#!/usr/bin/env bash
#
# usage example:
#   toolbox/scripts$ CHECKBOX_PPA=ppa:checkbox-dev/ppa ./cert-server-prepare.sh
#
# for checkbox dev PPA it is ppa:checkbox-dev/ppa
checkbox_ppa=${CHECKBOX_PPA-ppa:hardware-certification/public}

add-apt-repository -y ${checkbox_ppa}

apt-add-repository -y ppa:firmware-testing-team/ppa-fwts-stable

apt update

apt install -y canonical-certification-server

echo
echo "TEST_TARGET_IPERF is not your local IPs. Your local a.k.a. the testing target machine is a iperf client. Examples:"
echo
echo "sudo TEST_TARGET_IPERF=<IP1>[,<IP2>,...] certify-20.04"
echo "sudo TEST_TARGET_IPERF=<IP1>[,<IP2>,...] certify-soc-20.04"
echo
echo Example:
echo 'sudo TEST_TARGET_IPERF=10.229.70.195,192.168.5.2 certify-soc-20.04 > >(tee /home/ubuntu/cert-stdout.log) 2> >(tee /home/ubuntu/cert-stderr.log >&2)'
echo 'bg and ctrl-z'
echo 'tail -f /home/ubuntu/cert-stdout.log via ssh'
echo
echo "Using checkbox PPA: ${checkbox_ppa}"

