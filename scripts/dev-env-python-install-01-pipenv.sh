#/usr/bin/env bash
#
# Install Pipenv independent of the system python
#

PYPACKAGE="pipenv"

source "$(dirname "${BASH_SOURCE[0]}")"/dev-env-python-helper.sh

