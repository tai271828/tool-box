#!/usr/bin/env bash

#set -x
# trouble shooting
# python (bacterium's conflicted copy 2014-09-29).txt <--- this kind of zim file names will cause xargs crazy


string_to_remove_from_tag_cloud=$1
string_source="@${string_to_remove_from_tag_cloud}"
string_target="<this-AT-sign-not-a-zim-tag>${string_to_remove_from_tag_cloud}"

if [ -z "${string_to_remove_from_tag_cloud}" ]
then
    echo "You must input the first positional argument."
else
    echo "String the tag in the zim tag cloud...: ${string_to_remove_from_tag_cloud}"
    grep -l "${string_source} " -r $MYZIMNB | xargs -I % sed -i -e "s/${string_source} /${string_target} /" %
    grep -l "${string_source}#" -r $MYZIMNB | xargs -I % sed -i -e "s/${string_source}#/${string_target}#/" %
fi

