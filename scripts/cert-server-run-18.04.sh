echo "!!!You may want to run this with the console log in case ssh disconnection!!!"
echo
echo "...and source to run it and make it run in backgroupd (ctrl-z + bg)"
echo
echo "...in console, maybe source this script instead of simply executing it"
echo "(checkbox seems to return something during running in the backgroup to bring the process to fg and stop)"
echo
ip_iperf_servers="10.229.60.2,192.168.45.2"
sudo TEST_TARGET_IPERF=${ip_iperf_servers} certify-soc-18.04 > >(tee /home/ubuntu/cert-stdout.log) 2> >(tee /home/ubuntu/cert-stderr.log >&2) &
