#!/bin/bash

# http://alsa.opensrc.org/TroubleShooting#Check_that_the_ALSA_drivers_are_compiled_as_modules

# check driver
cat /proc/asound/version

# check card is active
cat /proc/asound/cards
