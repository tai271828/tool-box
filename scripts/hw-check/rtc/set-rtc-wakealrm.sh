#!/bin/bash

echo
echo
echo "setting the wakeup via /sys"
echo

echo "Before setting..."
cat /sys/class/rtc/rtc0/wakealarm
echo "cleaning..."
echo 0 > /sys/class/rtc/rtc0/wakealarm
echo "setting wakeup after 1 minutes..."
echo $(date +%s --date 'now + 1 minutes') > /sys/class/rtc/rtc0/wakealarm
echo "setting result..."
cat /sys/class/rtc/rtc0/wakealarm

echo "now system is going to shut down..."
shutdown -h now
