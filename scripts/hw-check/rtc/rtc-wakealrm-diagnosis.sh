#!/bin/bash

echo
echo
echo "run rtc-wakealrm-diagnosis.sh"
rtc_driver="/proc/driver/rtc"
wakealarm_ctl="/sys/class/rtc/rtc0/wakealarm"
grep rtc_time $rtc_driver
grep rtc_date $rtc_driver
grep alrm_time $rtc_driver
grep alrm_date $rtc_driver
grep alarm_IRQ $rtc_driver

cat $wakealarm_ctl

