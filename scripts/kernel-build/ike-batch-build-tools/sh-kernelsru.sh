#!/bin/bash

# parameter
if [ $# != 4 ]; then
	echo "Usage: $0 <project> <abibump> <upstream tag> <bug number>"
	echo "e.g. $0 keystone true Ubuntu-3.13.0-58.97 3334449"
	exit 1
fi
PROJECT="$1" # keystone or armadaxp
ABIBUMP="$2" # true or false, true if we need ABI number bumped
UPSTREAM="$3" # Upstream tag
LPNUMBER="$4" # Tracking bug number in launchpad

# Make sure this is an armhf platform
if [ "`dpkg-architecture | grep DEB_BUILD_ARCH=`" != "DEB_BUILD_ARCH=armhf" ]; then
	echo "On armhf platform please."
	exit 1
fi

# Clone git branch
WORKING_DIR=kernel_rebase_`date +%s`
if [ -d $WORKING_DIR ]; then
	echo "Please remove $WORKING_DIR folder."
	exit 1
fi
mkdir $WORKING_DIR
cd $WORKING_DIR
if [ $PROJECT == "keystone" ]; then
	git clone git://kernel.ubuntu.com/hwe/ubuntu-trusty-keystone.git linux-keystone-3.13.0
	wget https://launchpad.net/~canonical-kernel-team/+archive/ubuntu/ppa/+files/linux-keystone_3.13.0.orig.tar.gz
	cd linux-keystone-3.13.0
else
	git clone git://kernel.ubuntu.com/hwe/ubuntu-precise-armadaxp.git linux-armadaxp-3.2.0
	cd linux-armadaxp-3.2.0
fi
if [ ! -d .git ] || [ ! -d debian ]; then
	echo "git clone failed."
	exit 1
fi

# fetch from kernel.ubuntu.com
git remote add precise git://kernel.ubuntu.com/ubuntu/ubuntu-precise.git
git remote add trusty git://kernel.ubuntu.com/ubuntu/ubuntu-trusty.git
git fetch precise
git fetch trusty

# Get .orig.tar.gz

# Check if we have startnewrelease
source ./debian/debian.env
CHANGELOG=$DEBIAN/changelog
fakeroot debian/rules clean
KERNELVER=`debian/rules printenv | grep release | cut -d = -f 2 | cut -d \  -f 2`
OLD_ABINUM=`debian/rules printenv | grep abinum | cut -d = -f 2 | cut -d \  -f 2`
OLD_UPLOAD=`debian/rules printenv | grep uploadnum | cut -d = -f 2 | cut -d \  -f 2`
if [ "`grep CHANGELOG $CHANGELOG`" == "" ]; then
	# Clean debian folder first
	rm -fr debian*
	git checkout -f
	# Remove old ABI data
	git rm -r $DEBIAN/abi/3*
	# Get new ABI data
	fakeroot debian/rules clean
	debian/scripts/misc/getabis $KERNELVER $OLD_ABINUM.$OLD_UPLOAD
	git add $DEBIAN/abi/3*
	# if no ABI data received
	if [ "`ls $DEBIAN/abi/3*`" == "" ]; then
		mkdir abi-tmp-$KERNELVER-$OLD_ABINUM.$OLD_UPLOAD
		cd abi-tmp-$KERNELVER-$OLD_ABINUM.$OLD_UPLOAD
		wget https://launchpad.net/~canonical-kernel-team/+archive/ubuntu/ppa/+files/linux-image-$KERNELVER-$OLD_ABINUM-${PROJECT}_$KERNELVER-$OLD_ABINUM.${OLD_UPLOAD}_armhf.deb
		cd ..
		debian/scripts/misc/getabis $KERNELVER $OLD_ABINUM.$OLD_UPLOAD
		git add $DEBIAN/abi/3*
	fi
	# Add new changelog
	debian/rules startnewrelease
	if [ $ABIBUMP == "false" ]; then
		mv $CHANGELOG $CHANGELOG.tmp
		echo -ne "linux-$PROJECT ($KERNELVER-$OLD_ABINUM.$(($OLD_UPLOAD+1)))" > $CHANGELOG
		echo -ne " " >> $CHANGELOG
		echo -ne "`head -1 $CHANGELOG.tmp | cut -d \  -f 3`" >> $CHANGELOG
		echo -ne " " >> $CHANGELOG
		echo "`head -1 $CHANGELOG.tmp | cut -d \  -f 4`" >> $CHANGELOG
		tail -$((`wc -l $CHANGELOG.tmp | cut -d \  -f 1`-1)) $CHANGELOG.tmp >> $CHANGELOG
		rm -f $CHANGELOG.tmp
	fi
	git add $CHANGELOG
	# Write git commit
	git commit -s -F debian/commit-templates/newrelease
fi

# Rebase to specific tag
rm -fr debian*
git checkout -f
git rebase $UPSTREAM
debian/scripts/misc/insert-ubuntu-changes $CHANGELOG `git log --format="%s" | grep Rebase | head -1 | cut -d \- -f 3` `echo $UPSTREAM | cut -d \- -f 3`
COMMIT=`mktemp`
echo "UBUNTU: Rebase to $UPSTREAM" > $COMMIT
echo "" >> $COMMIT
git commit -s -F $COMMIT $CHANGELOG
rm -f $COMMIT

# Close commit
debian/rules insertchanges
mv $CHANGELOG $CHANGELOG.tmp
echo -ne "`head -1 $CHANGELOG.tmp | cut -d \  -f 1` " > $CHANGELOG
echo -ne "`head -1 $CHANGELOG.tmp | cut -d \  -f 2` " >> $CHANGELOG
echo -ne "`grep ^linux $CHANGELOG.tmp | head -2 | tail -1 | cut -d \  -f 3` " >> $CHANGELOG
echo "`head -1 $CHANGELOG.tmp | cut -d \  -f 4`" >> $CHANGELOG
head -4 $CHANGELOG.tmp | tail -3 >> $CHANGELOG
echo "  * Release Tracking Bug" >> $CHANGELOG
echo "    - LP: #$LPNUMBER" >> $CHANGELOG
tail -$((`wc -l $CHANGELOG.tmp | cut -d \  -f 1`-4)) $CHANGELOG.tmp >> $CHANGELOG
rm -f $CHANGELOG.tmp
COMMIT=`mktemp`
if [ $PROJECT == "keystone" ]; then
	echo "UBUNTU: keystone release Ubuntu-`head -1 $CHANGELOG | cut -d \( -f 2 | cut -d \) -f 1`" > $COMMIT
else
	echo "UBUNTU: Ubuntu-`head -1 $CHANGELOG | cut -d \( -f 2 | cut -d \) -f 1`" > $COMMIT
fi
echo "" >> $COMMIT
git commit -s -F $COMMIT $CHANGELOG
rm -f $COMMIT

# git tag
if [ $PROJECT == "keystone" ]; then
	TAGNAME="Ubuntu-`head -1 $CHANGELOG | cut -d \( -f 2 | cut -d \) -f 1`-keystone" 
else
	TAGNAME="Ubuntu-`head -1 $CHANGELOG | cut -d \( -f 2 | cut -d \) -f 1`" 
fi
git tag -s -m $TAGNAME $TAGNAME

# git push, please push it manually
echo git push origin \+$TAGNAME \+master

# Generate source package
rm -fr *
git checkout -f
fakeroot debian/rules clean
dpkg-buildpackage -S -rfakeroot -I.git -I.gitignore -i'\.git.*' -sa
cd ..
