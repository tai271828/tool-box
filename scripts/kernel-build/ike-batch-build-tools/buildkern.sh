#!/bin/bash

TARGET_BRANCH_NAME="${1:-master}"
WORKING_ROOT="$(dirname "${BASH_SOURCE[0]}")"

echo "Working on ${TARGET_BRANCH_NAME} branch"

echo "Restoring the source tree..."
git pull
git reset --hard ${TARGET_BRANCH_NAME}

for i in `seq 0 3`; do
    echo "Pre-build sanity for each commit..."
    git clean -df

    echo
    #git show --name-only
    git log -1
    echo

    echo "Appending the metadata to changelog"
    ${WORKING_ROOT}/sh-verappendtime.sh

    echo
    git --no-pager diff
    echo

    echo "Begin to build..."
    fakeroot debian/rules clean
    # binary-generic:
    #   image, modules, modules-extra
    #   buildinfo, headers, toolsi
    #
    # example:
    #   linux-buildinfo-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb
    #   linux-headers-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb 
    #   linux-image-unsigned-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb
    #   linux-modules-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb
    #   linux-modules-extra-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb
    #   linux-tools-5.13.0-22-generic_5.13.0-22.22~20.04.1_arm64.deb
    echo
    echo "========================================================="
    echo "Build binary-generic debs..."
    echo "========================================================="
    echo
    fakeroot debian/rules binary-generic skipabi=true skipmodule=true
    echo
    echo "========================================================="
    echo "Build with the return code $?"
    echo
    echo "Show the debs..."
    ls ../
    echo "========================================================="
    echo

    # binary-indep:
    #   cloud-tools-common, headers, source, tools
    #
    # example:
    #   linux-hwe-5.13-cloud-tools-common_5.13.0-22.22~20.04.1_all.deb
    #   linux-hwe-5.13-headers-5.13.0-22_5.13.0-22.22~20.04.1_all.deb
    #   linux-hwe-5.13-source-5.13.0_5.13.0-22.22~20.04.1_all.deb
    #   linux-hwe-5.13-tools-common_5.13.0-22.22~20.04.1_all.deb
    #   linux-hwe-5.13-tools-host_5.13.0-22.22~20.04.1_all.deb
    echo
    echo "========================================================="
    echo "Build binary-indep debs..."
    echo "========================================================="
    echo
    fakeroot debian/rules binary-indep
    echo
    echo "========================================================="
    echo "Build with the return code $?"
    echo
    echo "Show the debs..."
    ls ../
    echo "========================================================="
    echo

    fakeroot debian/rules clean

    echo
    echo "========================================================="
    echo "The reversed ${i} th commit has built (or fail to build)!"
    echo "========================================================="
    echo

    #git reset --hard HEAD~$i # remove # of patches from HEAD
    git reset --hard HEAD~1 # step back 1 patch per run
done

echo
echo "========================================================="
echo "Resume branch"
echo "========================================================="
echo
git pull

