#!/bin/bash

if [ ! -d debian ]; then
	echo "Not an Ubuntu kernel folder";
	exit 1;
fi

if [ ! -f debian/debian.env ]; then
	echo "No debian.env";
	exit 1;
fi

source debian/debian.env
CHANGELOG=$DEBIAN/changelog

SRCPKG=`sed -n "1s/^\(.*\) (.*).*$/\1/p" $CHANGELOG`
BASEVER=`sed -n "1s/^$SRCPKG.*(\(.*\)-.*).*$/\1/p" $CHANGELOG`
REVER=`sed -n "1s/^$SRCPKG\ .*($BASEVER-\(.*\)).*$/\1/p" $CHANGELOG`
TIME=`date +d%Y%m%dt%H%M%S`
LINE=`wc -l $CHANGELOG | cut -d \\  -f 1`
HASH=`git log --format="%h" | head -1`

rm -f $CHANGELOG.orig
mv $CHANGELOG $CHANGELOG.orig
if [ "$#" != "1" ]; then
	echo "$SRCPKG ($BASEVER-$REVER~$TIME~$HASH) TESTBUILD; urgency=low" > $CHANGELOG
else
	echo "$SRCPKG ($BASEVER-$REVER~$TIME~$HASH~$1) TESTBUILD; urgency=low" > $CHANGELOG
fi
tail -`expr $LINE - 1` $CHANGELOG.orig >> $CHANGELOG

cd $DEBIAN/rules.d
for i in `ls *.mk`; do
	echo "skipabi=true" >> $i;
	echo "skipmodule=true" >> $i;
done
