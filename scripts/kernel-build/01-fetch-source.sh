#!/usr/bin/env bash
git clone https://github.com/torvalds/linux mainline

# or ubuntu kernel team's repo git://kernel.ubuntu.com/ubuntu/ubuntu-focal.git
# it seems LP is the "official" one, for example, on 220111 we can only find jammy on LP
git clone --reference mainline https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/focal

