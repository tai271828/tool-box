#/usr/bin/env bash
#
# Install Poetry independent of the system python
#
target=${PYPACKAGE}

pushd $HOME

sudo apt-get update
sudo apt-get install -y python3-venv

mkdir -p ~/.local/bin ~/.local/venvs
python3 -m venv ~/.local/venvs/${target}
~/.local/venvs/${target}/bin/pip install ${target}
ln -s ~/.local/venvs/${target}/bin/${target} ~/.local/bin/

# post installation check
echo "Make sure ${HOME}/.local/bin is in your PATH"
export PATH=$HOME/.local/bin:$PATH

${target} --version

popd
