#!/usr/bin/env bash

source ./lxd-dhcp-vm-00-conf.sh
set +e

# testing
#lxc exec ${lxd_vm_0} bash

# tear-down after testing
lxc stop ${lxd_vm_0} ${lxd_vm_1}

lxc delete ${lxd_vm_0} ${lxd_vm_1}
lxc profile delete ${lxd_profile}
lxc network delete ${lxd_network}

lxc list | grep -e ${lxd_vm_0} -e ${lxd_vm_1}
lxc network list | grep ${lxd_network}

