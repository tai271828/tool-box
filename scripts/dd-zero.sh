echo "!!!You may want to run this with the console log in case ssh disconnection!!!"
echo
echo "...and run it in backgroupd (ctrl-z + bg)"
echo
sudo dd if=/dev/zero of=/dev/nvme0n1 status=progress > >(tee /home/ubuntu/dd-stdout.log) 2> >(tee /home/ubuntu/dd-stderr.log >&2)
