#!/usr/bin/env bash
#
# trigger by
# for i in {1..10}; do source /home/ubuntu/03-run-debug.sh ; done
#

log_dir_root=/home/ubuntu/run-debug-logs
mkdir -p ${log_dir_root}

# https://stackoverflow.com/questions/692000/how-do-i-write-stderr-to-a-file-while-using-tee-with-a-pipe
./03-run-debug-01-one-job.sh > >(tee ${log_dir_root}/${i}-stdout.log) 2> >(tee ${log_dir_root}/${i}-stderr.log >&2)

cp -a /home/ubuntu/autotest/client/results/default/ ${log_dir_root}/$i.logs
