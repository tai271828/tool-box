#!/bin/bash
#
# usage: invoke anywhere in the system by
#   ${HOME}/toolbox/scripts/${ACT_JOB}/02-setup-autotest-dgx.sh
#

set -e

toolbox_scripts=${HOME}/toolbox/scripts

source ${toolbox_scripts}/a-c-t-testing-00-general/02-setup-autotest-dgx-01.sh

# override the default value
ACT_REPO="https://gitlab.com/tai271828/autotest-client-tests.git"
ACT_BRANCH="dev-nvidia-fs-patch-enhancement"
ACT_JOB="a-c-t-testing-01-dl-performance"

source ${toolbox_scripts}/a-c-t-testing-00-general/02-setup-autotest-dgx-02.sh
