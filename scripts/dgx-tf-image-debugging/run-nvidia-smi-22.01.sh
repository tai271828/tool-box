#!/bin/bash
sudo nvidia-docker run \
     -it \
     --shm-size=1g \
     --ulimit memlock=-1 \
     --ulimit stack=67108864 \
     --rm nvcr.io/nvidia/tensorflow:22.01-tf1-py3 -- \
     nvidia-smi
