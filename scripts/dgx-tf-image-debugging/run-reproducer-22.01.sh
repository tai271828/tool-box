#!/bin/bash
sudo nvidia-docker run \
     --shm-size=1g \
     --ulimit memlock=-1 \
     --ulimit stack=67108864 \
     --rm nvcr.io/nvidia/tensorflow:22.01-tf1-py3 -- \
     mpiexec \
     --bind-to socket \
     --allow-run-as-root \
     -np "8" \
     python -u /workspace/nvidia-examples/cnn/resnet.py \
     --layers=50 \
     --precision=fp16 \
     --batch_size=256 \
     --num_iter=300 \
     --iter_unit=batch \
     --display_every=300
