#!/usr/bin/env bash
dir_name=canonical-server-hwe-utils
dir_path=${HOME}/work/${dir_name}
PYPACKAGE="poetry"

git clone -b main https://git.launchpad.net/canonical-server-hwe-utils

source ${HOME}/toolbox/scripts/dev-env-python-helper.sh

echo 'source ${HOME}/toolbox/config/bash/hook-all.sh' >> ${HOME}/.bash_aliases

mkdir -p ${HOME}/.local/bin
pushd ${HOME}/.local/bin
ln -vnfs /home/tai271828/toolbox/scripts/labkey ./
popd

${HOME}/.local/venvs/poetry/bin/pip install pipenv
ln -vnfs ${HOME}/.local/venvs/poetry/bin/pipenv ~/.local/bin/
