#!/usr/bin/env bash

#source ./activate-modmesh-env.sh

which python3

make clean
make cmakeclean
#rm -f _modmesh.cpython-3*-x86_64-linux-gnu.so

set -e

make buildext
#make pytest
#make viewer VERBOSE=1 BUILD_QT=ON CMAKE_BUILD_TYPE=Debug CMAKE_ARGS="-DPYTHON_EXECUTABLE=$(which python3)"

# run the viewer
#./build/dbg39/cpp/binary/viewer/viewer

make install
