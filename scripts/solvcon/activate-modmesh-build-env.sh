export VERBOSE=1

export CMAKE_PREFIX_PATH=${HOME}/Qt/6.2.4/gcc_64:${HOME}/work-my-projects/solvcon/modmesh-installation/lib/cmake/easy_profiler
export CMAKE_BUILD_TYPE=Debug
export CMAKE_ARGS="-DPYTHON_EXECUTABLE=$(which python3)"

export MODMESH_PROFILE=ON
#export MODMESH_USE_PYSIDE=True
