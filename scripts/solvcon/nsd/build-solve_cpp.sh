# use cmake to build xtl, xtensor, xtensor-python
# mkdri build
# cd build
# cmake -DCMAKE_INSTALL_PREFIX=${HOME}/work-my-projects/solvcon/modmesh-installation ../
# make install
#
# debugger: vscode + "Bash Debug" extension by rogalmic
poetry_python_venv="${HOME}/.cache/pypoetry/virtualenvs/modmesh-venv-py3-9-13-LgJ7FNfF-py3.9"
INC="-I${HOME}/work-my-projects/solvcon/modmesh-installation/include \
    -I/usr/include/python3.9 \
    -I${poetry_python_venv}/lib/python3.9/site-packages/numpy/core/include"
LIB="-L${poetry_python_venv}/lib"

# -g for debug symbol (useful when launching gdb python)
# -O0 for more debugging info (less optimized variables)
CMD="g++ -g solve_cpp.cpp -o solve_cpp.so -O0 -fPIC -shared -std=c++17 -lpython3.9 ${LIB} ${INC}"

${CMD}
