#!/usr/bin/env bash
#
# usage: invoke anywhere in the system by
#   ${HOME}/toolbox/scripts/solvcon/modmesh.sh
#
# Tested platforms: ubuntu jammy (22.04)
#

# apt zlib
sudo apt-get -qqy update
# TODO: may need to be non-interactive
# some dependencies bring up questions pending to answer in Ubuntu Jammy
sudo apt-get -qqy install fakeroot debhelper locales unzip \
                          libreadline-dev libssl-dev libffi-dev \
                          liblapack-dev libhdf5-dev libnetcdf-dev libscotch-dev \
                          libpython3-dev python3-boto python3-paramiko graphviz
# for cmake?
sudo apt install -y ninja-build

# TODO: some of them should be redundant
# core is not sufficient. If there is only core, you may have error
#   like Unknown CMake command "qt_add_executable"

sudo apt install -y libqt6core6 qt6-base-dev qt6-base-dev-tools \
                    qt6-quick3d-dev qt6-quick3d-dev-tools qt6-3d-dev libqt63dcore6 \
                    libxkbcommon-dev
#sudo apt install -y libqt6webenginewidgets6

# Fix:
#CMake Error at cpp/binary/viewer/CMakeLists.txt:50 (target_link_libraries):
#  Target "viewer" links to:
#
#    Qt::Widgets
#
#  but the target was not found.  Possible reasons include:
#
#    * There is a typo in the target name.
#    * A find_package call is missing for an IMPORTED target.
#    * An ALIAS target is missing.
sudo apt-get install build-essential libgl1-mesa-dev

# TODO: these packages should be redundant
sudo apt install -y qml-qt6
# TODO: may move to python venv
sudo apt install -y flake8

git clone https://github.com/solvcon/devenv.git
git clone https://github.com/solvcon/modmesh.git

pushd devenv
source scripts/init
devenv add prime
devenv use prime
devenv launch solvcon
popd

# You can disable to build modmesh with Qt6 via:
#
#~/modmesh$ git diff
#diff --git a/CMakeLists.txt b/CMakeLists.txt
#index 6a60e0c..0e0f1fc 100644
#--- a/CMakeLists.txt
#+++ b/CMakeLists.txt
#@@ -11,7 +11,7 @@ if(NOT SKIP_PYTHON_EXECUTABLE)
#     message(STATUS "use PYTHON_EXECUTABLE=${PYTHON_EXECUTABLE}")
# endif()
#
#-option(BUILD_QT "build QT" ON)
#+option(BUILD_QT "build QT" OFF)
#
## prepare QT6 for recent modmesh tree after commit ed59a256f624dd99ac052ff7b044c0510eb2a7e4 (or later?? before
## QT-related commits anyway)
##
## ref: https://wiki.qt.io/Building_Qt_6_from_Git
#pushd /tmp
#git clone https://code.qt.io/qt/qt5.git
#cd qt5
#git checkout v6.2.4
#perl init-repository
#
#mkdir -p ../qt6-build
#cd qt-build
#../qt5/configure -prefix "${LD_LIBRARY_PATH}"
#cmake --build . --parallel 4
#cmake --install .
#popd

pushd ${HOME}/modmesh
# do not build with Qt6
#python3 setup.py build_ext --cmake-args="-DBUILD_QT=OFF -DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=$(which python3)" --make-args="VERBOSE=1"
#python3 setup.py build_ext --cmake-args="-DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=$(which python3)" --make-args="VERBOSE=1"
#python3 setup.py install
cmake -DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=$(which python3)
make VERBOSE=1
make viewer

# hmm installation is incorrect? /usr/local/viewer/viewer which should be /usr/local/bin/viewer ?
# besides, why not in devenv flavor?
#sudo make install

# will not pass if building via cmake directly but not python3 setup.py
#pip3 install pytest
#pytest -v
popd
