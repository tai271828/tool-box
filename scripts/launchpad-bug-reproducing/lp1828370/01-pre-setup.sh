./collect-system-info.sh > ${HOME}/collect-system-info-01-pre-setup.log 2>&1
ln -s $(pwd) ${HOME}/

sudo passwd ubuntu

wget http://cdimage.ubuntu.com/releases/20.04.1/release/ubuntu-20.04.1-live-server-arm64.iso -O ${HOME}/ubuntu-20.04.1-live-server-arm64.iso

local_release=$(lsb_release --short --codename)
if [ ${local_release} == "bionic" ]; then
    sudo apt-get install qemu-kvm qemu-efi virtinst libvirt-bin -y
elif [ ${local_release} == "focal" ]; then
    sudo apt-get install qemu-kvm qemu-efi virtinst libvirt-daemon-system libvirt-clients -y
else
    sudo apt-get install qemu-kvm qemu-efi virtinst libvirt-bin -y
fi

echo "=================================================="
echo "Re-loggin to make the libvirt take effect!!!!!!!!!"
echo "=================================================="
