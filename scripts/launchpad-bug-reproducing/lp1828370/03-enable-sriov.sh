interface="enp133s0f0"
sriov_enable=3

echo "before enabling:"
cat /sys/class/net/${interface}/device/sriov_numvfs
echo ${sriov_enable} | sudo tee /sys/class/net/${interface}/device/sriov_numvfs
echo "after enabling:"
cat /sys/class/net/${interface}/device/sriov_numvfs
