#!/bin/bash
#macaddr=$(echo $HOSTNAME | md5sum | sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')
macaddr=02:40:2c:f2:7e:75
cloud_image="cloudimg.img"
memory=8192

sudo qemu-system-aarch64 \
    -enable-kvm -M virt \
    -m $memory -cpu host \
    -nographic \
    -pflash flash0.img -pflash flash1.img \
    -drive if=none,file=$cloud_image,id=hd0 \
    -device virtio-blk-device,drive=hd0 \
    -drive file=user-data.img,format=raw \
    -netdev type=tap,id=net1,ifname=tap-dhcp-1,script=no,downscript=no \
    -device virtio-net-device,netdev=net1,mac=$macaddr
