#!/bin/bash
#
# usage: invoke anywhere in the system by
#   ${HOME}/toolbox/scripts/a-c-t-testing-03-nv-server-driver/02-setup-autotest-dgx.sh
#

set -e

pushd ${HOME}

git clone --depth=1 ${ACT_REPO} -b ${ACT_BRANCH}
git clone --depth=1 git://kernel.ubuntu.com/ubuntu/autotest

rm -rf autotest/client/tests
ln -sf ~/autotest-client-tests autotest/client/tests

ln -sf ~/toolbox/scripts/${ACT_JOB}/03-run-debug.sh ~/
ln -sf ~/toolbox/scripts/${ACT_JOB}/03-run-debug-01-one-job.sh ~/

popd
