#!/bin/bash
interface_bridge=bridge-dhcp
interface_tap_node_0=tap-dhcp-0
interface_tap_node_1=tap-dhcp-1
subnet_tap_node_0="192.10.0/24"

create_tap() {
    local interface_tap=$1
    local subnet=$2

    ip tuntap add $interface_tap mode tap user root
    if [ -n "$subnet" ]; then
        ip addr add dev $interface_tap $subnet
    fi
    ip link set $interface_tap up
}

create_tap $interface_tap_node_0 $subnet_tap_node_0
create_tap $interface_tap_node_1

brctl addbr $interface_bridge

brctl addif $interface_bridge $interface_tap_node_0
brctl addif $interface_bridge $interface_tap_node_1

