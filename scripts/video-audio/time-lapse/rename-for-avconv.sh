#!/bin/bash
#
# after renaming, merge jpg into mkv video by
# avconv -i 'img-%05d.jpg' -r 10 out.mkv
#

# generate by ls *.jpg > files.txt
filename='files.txt'
ls *.jpg > $filename
exec < $filename

series_number=1

while read line
do
    filename_new=img-`printf %05g ${series_number}`.jpg
    echo $line ${filename_new}
    mv $line $filename_new
    series_number=$((${series_number}+1))
done

rm $filename

echo
echo "now please run the command:"
echo "    avconv -i 'img-%05d.jpg' -r 10 out.mkv"
echo

