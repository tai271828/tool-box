#!/usr/bin/python

import pdb

time_shift_hour = 1 # in hour
time_shift_sec = 8 # in sec

input_filename = 'fresh_sub_ch.txt'
output_filename = input_filename + '.srt'

input_filename_init = input_filename + '.init'

list_time = []
list_sub = []

list_final_time = []
list_final_sub = []


def get_shift_time(time):
    # 01.02.19
    # 00:02:11
    hms = time.split('.')
    shifted_time_in_sec =   int(hms[0])*60*60 + int(hms[1])*60 + int(hms[2]) \
                            - time_shift_hour*60*60 - time_shift_sec
    h =  (shifted_time_in_sec - (shifted_time_in_sec % 3600))/3600
    m = ((shifted_time_in_sec - (shifted_time_in_sec %   60))/  60) - h*60
    s =   shifted_time_in_sec % 60

    hh = str(h).zfill(2)
    mm = str(m).zfill(2)
    ss = str(s).zfill(2)

    return hh + ':' + mm + ':' + ss

def time_str_to_sec_int(time):
    hms = time.split(':')
    shifted_time_in_sec =   int(hms[0])*60*60 + int(hms[1])*60 + int(hms[2])
    return shifted_time_in_sec

def sec_int_to_time_str(shifted_time_in_sec):
    h =  (shifted_time_in_sec - (shifted_time_in_sec % 3600))/3600
    m = ((shifted_time_in_sec - (shifted_time_in_sec %   60))/  60) - h*60
    s =   shifted_time_in_sec % 60

    hh = str(h).zfill(2)
    mm = str(m).zfill(2)
    ss = str(s).zfill(2)

    return hh + ':' + mm + ':' + ss
    

def get_srt():
    with open(input_filename) as input_file, open(output_filename, 'w') as output_file :
        data = input_file.readlines()
        for line in data:
            hour_min_sec =  line.split("\t")[0].rstrip()
            long_sub =      line.split("\t")[1].rstrip()

            output_file.write(str(data.index(line)) +"\n")
            srt_body = hour_min_sec + ',000 --> ' + hour_min_sec + ',900' + "\n"
            output_file.write(srt_body + long_sub + "\n\n")

tmp_filename = output_filename
output_filename = input_filename_init
with open(input_filename) as input_file, open(output_filename, 'w') as output_file :
    data = input_file.readlines()
    for line in data:
        hour_min_sec = get_shift_time(line.split("\t")[0].rstrip())
        long_sub = line.split("\t")[1].strip()
        output_file.write(hour_min_sec + "\t" + long_sub + "\n")
output_filename = tmp_filename


input_filename = input_filename_init
with open(input_filename) as input_file, open(output_filename, 'w') as output_file :
    data = input_file.readlines()
    for line in data:
        hour_min_sec =  line.split("\t")[0].rstrip()
        long_sub =      line.split("\t")[1].rstrip()

        long_sub_tmp = long_sub.split(' ')
        long_sub_tmp_candi = []
        for x in long_sub_tmp:
            if len(x) > 1:
                long_sub_tmp_candi.append(x)
        long_sub_several_sentence = long_sub_tmp_candi

        # Debug.
        ss = ''
        for x in long_sub_several_sentence:
            ss = ss + x.rstrip() + ' '
        output_file.write(hour_min_sec + "\t" + ss.rstrip() + "\n")

        # not done. is bad
        #if len(long_sub_several_sentence) == 1:
        #    output_file.write(hour_min_sec + "\t" + long_sub_several_sentence[0] + "\n")
        #elif len(long_sub_several_sentence) > 1:
        #    slice_number = len(long_sub_several_sentence)
        #    line_index_in_data = data.index(line)
        #    total_sec_start = time_str_to_sec_int(hour_min_sec)
        #    next_hms = data[line_index_in_data+1].split("\t")[0].rstrip()
        #    print next_hms
        #    total_sec_end =   time_str_to_sec_int(next_hms)
        #    total_sec_delta = (total_sec_end - total_sec_start)/slice_number
        #    for x in xrange(slice_number):
        #        output_file.write(sec_int_to_time_str(total_sec_start + total_sec_delta*x) \
        #                + "\t" \
        #                + long_sub_several_sentence[x] \
        #                + "\n")
        #else:
        #    print 'sth. wrong'

get_srt()
