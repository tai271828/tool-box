#!/usr/bin/env bash
#
# build Ubuntu kernel with Ubuntu Focal
#
pkgs_kernel_build_dep_extra="libncurses-dev gawk flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf"
pkgs_tools_deb="debhelper"
pkgs_tools_extra="fakeroot"
#pkgs_dpkg_installation="linux-hwe-5.11-tools-5.11.0-22 linux-hwe-5.11-headers-5.11.0-22 linux-tools-common"
pkgs="${pkgs_kernel_build_dep} ${pkgs_kernel_build_dep_extra} ${pkgs_tools_deb} ${pkgs_tools_extra} ${pkgs_dpkg_installation}"

# enable deb-src
sudo sed -i '/^#\sdeb-src /s/^#//' "/etc/apt/sources.list"

sudo apt-get update

sudo apt build-dep -y linux linux-image-$(uname -r)
sudo apt install -y ${pkgs}

# TODO: when downloading kernel source from LP, 1) use swallow git depth --depth 2) and multi core like -j4
git clone -b Ubuntu-5.15.0-27.28 --depth 1 --jobs 4 https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/jammy

