# usage: source me
#
# Activate the corresponding conda venv. This script is usually used by the
# other script providing the practical PROJECT_ROOT.
#

# Edit me. Specify your virtual environment folder path.
PROJECT_DEV_VENV="${PROJECT_WORKING_ROOT}/venv"

eval "$(${MINICONDA_BASE}/bin/conda shell.bash hook)"
#conda create -p ${PROJECT_DEV_VENV} --no-default-packages -y python
conda activate ${PROJECT_DEV_VENV}

