#!/usr/bin/env bash
source ./basic.sh

set -x
sudo grep "current version of snap is" /var/log/installer/subiquity-server-debug.log
echo
dpkg-query --list --no-pager | grep -e nvidia
echo
dpkg-query --list --no-pager | grep -e nvidia-fabricmanager
echo
ubuntu-drivers list --gpgpu
echo
ubuntu-drivers list --gpgpu --recommended
set +x
