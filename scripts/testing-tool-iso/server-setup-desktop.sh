# will be very useful for vmedia cd testing
#
# 18.04 is suggested
#
sudo apt update
sudo apt install vnc4server xfce4 xfce4-goodies chromium-browser -y

vncpasswd

mkdir -p ${HOME}/.vnc
cat << EOF >> ${HOME}/.vnc/xstartup
#!/bin/bash
startxfce4&
EOF
chmod +x ${HOME}/.vnc/xstartup

vnc4server -geometry 1920x1080

mkdir ${HOME}/jammy-daily-live-iso
pushd ${HOME}/jammy-daily-live-iso
#wget https://cdimage.ubuntu.com/ubuntu-server/daily-live/current/jammy-live-server-arm64.iso
#wget https://cdimage.ubuntu.com/ubuntu-server/daily-live/current/jammy-live-server-amd64.iso
wget https://cdimage.ubuntu.com/ubuntu-server/daily-live/20220415/jammy-live-server-amd64.iso
popd
