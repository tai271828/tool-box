#kernel=4.15.0-101
kernel=$1

sudo apt-get update
sudo apt-get install linux-image-${kernel}-generic linux-modules-${kernel}-generic linux-modules-extra-${kernel}-generic -y

awk -F\' '/menuentry / {print $2}' /boot/grub/grub.cfg

echo
echo "Use the following command to boot with the target kernel:"
echo "The index starts from 0 for the main and each sub menu."
echo
echo 'sudo grub-reboot "1>2"'
echo

