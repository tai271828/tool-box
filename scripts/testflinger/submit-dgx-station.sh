#!/usr/bin/env bash
JOB_YAML="dgx-station.recipe.yaml"

JOB_ID=$(testflinger-cli submit -q ${JOB_YAML})
echo "JOB_ID: ${JOB_ID}"

testflinger-cli poll ${JOB_ID}

TEST_STATUS=$(testflinger-cli results ${JOB_ID} |jq -r .test_status)

testflinger-cli artifacts ${JOB_ID} || (sleep 30 && testflinger-cli artifacts ${JOB_ID})

