#!/usr/bin/env bash
#
# short turn-around time of iperf testing
#
ip_0=10.229.44.172
ip_1=192.168.45.2
interface_to_test_0=enp1s0f0
interface_to_test_1=enp1s0f1

echo "on testing iperf3..."
echo "make sure"
echo "    1. seperated subnets for testing"
echo "    2. enabl iperf3 with sufficient processes"
echo "    3. !!!!!! invoke this command in serial console rather than ssh session!!!!"


echo
echo "testing ${interface_to_test_0}"
echo "should only ${ip_0} able to communicate"
sudo TEST_TARGET_IPERF=${ip_0},${ip_1} /usr/lib/plainbox-provider-checkbox/bin/network.py test -i ${interface_to_test_0} -t iperf --iperf3 --scan-timeout 5 --fail-threshold 80 --cpu-load-fail-threshold 90 --runtime 2 --num_runs 2

echo
echo "testing ${interface_to_test_1}"
echo "should only ${ip_1} able to communicate"
sudo TEST_TARGET_IPERF=${ip_0},${ip_1} /usr/lib/plainbox-provider-checkbox/bin/network.py test -i ${interface_to_test_1} -t iperf --iperf3 --scan-timeout 5 --fail-threshold 80 --cpu-load-fail-threshold 90 --runtime 2 --num_runs 2
