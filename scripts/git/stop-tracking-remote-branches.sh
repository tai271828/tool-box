#!/bin/bash
#
# auxiliary command:
#     git branch --all | grep remotes | grep upstream | awk -F' remotes/' {'print $2'} > ./branches-upstream.txt
#     cat branches-upstream.txt | xargs ./stop-tracking-remote-branches.sh
#

target_branches="$@"
echo ${target_branches}
echo

for branch in "${target_branches}"
do
    echo "Removing ${branch}"
    git branch -d -r upstream ${branch}
done
