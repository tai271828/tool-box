#!/bin/bash
#
# auxiliary command:
#     git branch --all | grep remotes | grep upstream | awk -F' /' {'print $3'} > ./branches.txt
#     cat branches-upstream.txt | xargs ./prune-remote-branches.sh
#
# or, try:
#    git remote prune --dry-run origin
#

target_branches="$@"
echo ${target_branches}
echo

for branch in "${target_branches}"
do
    echo "Removing ${branch}"
    git push -d origin ${branch}
done
