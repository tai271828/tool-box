#sudo lldpad -d
#sleep 1 
sudo lldptool -L -i $1 adminStatus=rxtx
sleep 1

sudo lldptool -T -i $1 -V ETS-CFG tsa=0:ets,1:ets,2:ets,3:ets,4:ets,5:ets,6:ets,7:ets up2tc=0:0,1:0,2:1,3:1,4:2,5:2,6:3,7:3 tcbw=25,25,50,0,0,0,0,0
sleep 1

sleep 1
ip link show $1
echo

sudo ip link set dev $1 down
sleep 3
ip link show $1
echo

sudo ip link set dev $1 up
sleep 3
ip link show $1
