#!/bin/bash
devices_up=$(ip a | grep 'state UP' | awk -F': ' {'print $2'})
devices_sriov=$(ls /sys/class/net/*/device/sr* | awk -F'/device/sriov' {'print $1'} | uniq | awk -F'/sys/class/net/' {'print $2'})

for up_device in ${devices_up}
do
    for sriov_device in ${devices_sriov}
    do
	if [ "${up_device}" == "${sriov_device}" ]
	then
	    echo ${up_device}
	fi
    done
done
