#!/bin/bash
commits="f8408d2b79b834f79b6c578817e84f74a85d2190 bedd04e4aa1434d2f0f038e15bb6c48ac36876e1 34932a6033be3c0088935c334e4dc5ad43dcb0cc"
# 2 test commits, one should show up and the other should not
#commits="f8408d2b79b834f79b6c578817e84f74a85d2190 bedd04e4aa1434d2f0f038e15bb6c48ac36876e1 34932a6033be3c0088935c334e4dc5ad43dcb0cc e7bfd4eafb5ffec1b44ab906b3d5f22325abbb53 c8b819d0bbc72eeda1e9fe3807e2f91d0efb7082"

for commit in ${commits}
do
    echo ${commit}
    # the upstream format
    get_git_tag_by_hash 'v[[:digit:]]*' ${commit} 'v[[:digit:]]*'
    # hwe-5.11
    #get_git_tag_by_hash 'Ubuntu-hwe-5.11-[[:digit:]]*' ${commit} '^Ubuntu-hwe-*+\_+*'
    echo
done
