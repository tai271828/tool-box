#!/bin/bash
#
# Please note this script only works for bionic or later. For earlier version
# you may use the following packages instead:
#
# linux-headers-$KVERSION linux-headers-$KVERSION-generic linux-image-$KVERSION-generic
# linux-image-extra-$KVERSION-generic linux-signed-image-$KVERSION-generic
#
# kernel version to be installed and boot to
KVERSION="5.4.0-52"

sudo apt-get update
sudo apt-get install -y \
    linux-headers-${KVERSION} \
    linux-headers-${KVERSION}-generic \
    linux-image-${KVERSION}-generic \
    linux-modules-${KVERSION}-generic \
    linux-modules-extra-${KVERSION}-generic

sudo sed -i "s/GRUB_DEFAULT=.*/GRUB_DEFAULT=\"Advanced options for Ubuntu>Ubuntu, with Linux ${KVERSION}-generic\"/" /etc/default/grub
sudo update-grub

sudo reboot
