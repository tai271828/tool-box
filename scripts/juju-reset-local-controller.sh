# remove the controller info managed by juju (we can kill lxd containers manually)
# you can find IP and port of the controller here
rm -rf ~/.local/share/juju/
# re-bootstrap to get a clean and working juju controller
juju bootstrap localhost
