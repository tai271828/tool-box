#!/bin/bash

python() {
    local PYTHON="$(which python)"
    if [[ "$PYTHON" == /usr/* ]]; then
        echo "Only system python is available. Stop." >&2 | echo > /dev/null
    else
        "$PYTHON" "$@"
    fi
}

python3() {
    local PYTHON="$(which python3)"
    if [[ "$PYTHON" == /usr/* ]]; then
        echo "Only system python3 is available. Stop." >&2 | echo > /dev/null
    else
        "$PYTHON" "$@"
    fi
}

# activate pyenv if we want to use pyenv and the shim
#export PATH="$HOME/.pyenv/bin:$PATH"
#eval "$(pyenv init -)"
