ip_iperf_servers="10.229.58.10,192.168.45.2"
job_list="com.canonical.certification::device"
job_list="${job_list} com.canonical.certification::ethernet/multi_iperf3_nic_device1_enp1s0f0"
log_stdout=${HOME}/iperf-stdout.log
log_stderr=${HOME}/iperf-stderr.log

sudo TEST_TARGET_IPERF=${ip_iperf_servers} checkbox-cli run ${job_list} > >(tee ${log_stdout}) 2> >(tee ${log_stderr} >&2)
