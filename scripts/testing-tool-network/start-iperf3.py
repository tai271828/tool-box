#!/usr/bin/env python3

# Copyright (c) 2018 Canonical, Ltd.
#
# Authors
#   Rod Smith <rod.smith@canonical.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# On Debian systems, the complete text of the GNU General Public
# License, version 3, can be found in /usr/share/common-licenses/GPL-3.
#
# Program to launch multiple iperf3 instances for testing of high-speed
# network devices using the Checkbox "network" script. Each instance is
# bound to a single IP address, is tied to a specific CPU core, and has
# its own port number (starting with 5201, the default iperf3 port number).


# usage: python3 ./start-iperf3.py -a 192.168.122.1 -n 3
# each one for 10G so 3 for 30G
# trigger twice for two differnt IPs?

from argparse import ArgumentParser
import psutil
import re
import shlex
from subprocess import (
    CalledProcessError,
    check_output,
    STDOUT
)
import sys


def find_numa(device):
    """Return the NUMA node of the specified network device."""
    filename = "/sys/class/net/" + device + "/device/numa_node"
    try:
        file = open(filename, "r")
        node_num = int(file.read())
    except FileNotFoundError:
        print("WARNING: Could not find the NUMA node associated with {}!".
              format(device))
        print("Setting the association to NUMA node 0, which may not be "
              "optimal!")
        node_num = 0
    # Some systems (that don't support NUMA?) produce a node_num of -1.
    # Change this to 0, which seems to be correct....
    if node_num == -1:
        node_num = 0
    print("NUMA node of {} is {}....".format(device, node_num))
    return node_num


def find_device(ipaddr):
    """Return the device name (e.g., eth0) of the specified IP address."""
    adapters = psutil.net_if_addrs()
    found_nic = None
    for adapter in adapters:
        address = adapters[adapter][0].address
        if address == ipaddr:
            found_nic = adapter
    return found_nic


def extract_core_list(line):
    """Extract a list of CPU cores from a line of the form:
    NUMA node# CPU(s):    a-b[,c-d[,...]]"""
    colon = line.find(":")
    cpu_list = line[colon+1:]
    core_list = []
    for core_range in cpu_list.split(","):
        # core_range should be of the form "a-b" or "a"
        range_list = core_range.split("-")
        if len(range_list) > 1:
            # core_range was a range ("a-b"), so we must add all cores in
            # that range....
            for i in range(int(range_list[0]), int(range_list[1]) + 1):
                core_list.append(i)
        elif len(range_list) == 1:
            # core_range was a single CPU, so add just it....
            core_list.append(int(range_list[0]))
        else:
            # Weirdness, so alert the user....
            print("Cannot parse CPU list:")
            print(cpu_list)
    print("Will use CPU cores: {}....".format(core_list))
    return core_list


def find_cores(numa_node):
    """Return a list of CPU cores associated with the specified NUMA node."""
    numa_return = check_output("lscpu", universal_newlines=True,
                               stderr=STDOUT).split("\n")
    expression = "NUMA node.*" + str(numa_node) + ".*CPU"

    regex = re.compile(expression)
    core_list = []
    if numa_return:
        for i in numa_return:
            hit = regex.search(i)
            if hit:
                # Some testing values; uncomment to force different
                # values for testing....
                # i = "0-3,8-11"
                # i = "0"
                core_list = extract_core_list(i)
    return core_list


def launch_all_iperf3(core_list, ip_address, num_instances):
    """Launch the actual iperf3 instances."""
    start_port = 5201
    if num_instances > len(core_list):
        print("WARNING: Number of instances exceeds number of available\n"
              "cores on the NUMA node associated with the network device.\n"
              "Performance may be degraded!")
    for thread_num in range(0, num_instances):
        port = start_port + thread_num
        core = core_list[thread_num % len(core_list)]
        cmd = "iperf3 -sD -B {} -p {} -A{}".format(ip_address, port, core)
        print("Launching: {}".format(cmd))
        try:
            check_output(shlex.split(cmd), universal_newlines=True)
        except CalledProcessError as iperf_exception:
            print("Failed running iperf: {}".format(iperf_exception.output))


def main(args):
    gopts = ArgumentParser()
    gopts.add_argument('-a', '--address', type=str,
                       help="The IP address on which iperf3 should run",
                       required=True, default=None)
    gopts.add_argument("-n", "--num-instances", type=int,
                       help="The number of iperf3 instances to run",
                       required=False, default=10)

    args = gopts.parse_args()

    device = find_device(args.address)
    if device is not None:
        node = find_numa(device)
        core_list = find_cores(node)
        launch_all_iperf3(core_list, args.address, args.num_instances)
    else:
        print("Could not find a network device associated with {}!\n".
              format(args.address))


if __name__ == "__main__":
    sys.exit(main(sys.argv))

