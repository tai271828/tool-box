#!/usr/bin/env bash
source dev-env-python-install-01-pipenv.sh
source dev-env-python-install-02-python-build-pyenv.sh
#source dev-env-python-install-02-python-build-standalone.sh
source dev-env-python-install-03-dep-build-python.sh
source dev-env-python-install-04-poetry.sh

echo
echo "You may want to:"
echo "    1. Hide your system-default Python."
echo "       Refer to dev-env-python-hide-system-py.sh and put it into your .bash_alias or something."
echo "    2. make ~/.local/bin in your login conf."
echo

echo "Quick check of the installation"
export PATH=${HOME}/.local/bin:$PATH
which python
python --version

whicn python3
python3 --version

which pipenv
pipenv --version

which poetry
poetry --version

