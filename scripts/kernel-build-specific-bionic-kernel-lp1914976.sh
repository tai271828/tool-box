source_tree="https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic"
source_directory="bionic"
kernel_version_tag="Ubuntu-hwe-5.0.0-23.24_18.04.1"
config_template="/boot/config-5.4.0-66-generic"
pkgs_extra="libncurses-dev gawk flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf"

git clone ${source_tree}

# fetch build helper tools
sudo apt-add-repository ppa:dannf/dannf -y
sudo apt-get update
sudo apt install -y dannf
dannf-kernel-dev-setup.sh

# fetch more build dependency
sudo apt-get update
sudo apt-get build-dep linux linux-image-$(uname -r) -y
sudo apt-get install ${pkgs_extra} -y

# checkout the target source
cd ${source_directory}
git checkout Ubuntu-hwe-5.0.0-23.24_18.04.1
cp ${config_template} .config
dannf-build-kernel.sh
