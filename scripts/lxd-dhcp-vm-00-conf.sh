#!/usr/bin/env bash

set -e

lp_suffix="lp1981644"
lxd_profile="prof"-${lp_suffix}
lxd_network="netw"-${lp_suffix}
lxd_vm_0="node-0"-${lp_suffix}
lxd_vm_1="node-1"-${lp_suffix}

