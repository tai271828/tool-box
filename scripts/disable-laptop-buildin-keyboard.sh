device_ip=`xinput --list | grep "AT Translated" | awk '{print $7}' | awk -F= '{print $2}'`
# disable the device
xinput set-int-prop ${device_ip} "Device Enabled" 8 0
# enable the device
#xinput set-int-prop ${device_ip} "Device Enabled" 8 1
