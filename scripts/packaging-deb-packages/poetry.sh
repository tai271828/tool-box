#!/usr/bin/env bash
#
# tested distribution: ubuntu impish (21.10), ubuntu focal (20.04)
#
WORKING_ROOT=${HOME}/build-poetry-deb
EXTRA_PACKAGE_ROOT=${WORKING_ROOT}/DEPENDENCIES
POETRY_SRC=${WORKING_ROOT}/poetry
PKG_PY3_HTTPRETTY_NAME="python3-httpretty_1.1.4-1_all.deb"

mkdir -p ${WORKING_ROOT}
cd ${WORKING_ROOT}
git clone https://salsa.debian.org/python-team/packages/poetry.git

mkdir -p ${EXTRA_PACKAGE_ROOT}
cd ${EXTRA_PACKAGE_ROOT}
wget http://ftp.debian.org/debian/pool/main/p/python-httpretty/${PKG_PY3_HTTPRETTY_NAME}
wget http://ftp.debian.org/debian/pool/main/p/python-pkginfo/python3-pkginfo_1.4.2-3_all.deb http://ftp.debian.org/debian/pool/main/p/python-cleo/python3-cleo_0.8.1-2_all.deb

sudo apt update
sudo apt install git-buildpackage sbuild debhelper dh-python -y

sudo sbuild-adduser ubuntu
cp /usr/share/doc/sbuild/examples/example.sbuildrc /home/ubuntu/.sbuildrc

sudo debootstrap sid /var/chroot/sid http://deb.debian.org/debian/
#sudo cp /etc/schroot/schroot.conf /etc/schroot/chroot.d/poetry-build.conf
cat << EOT >> /tmp/poetry-build.conf
[unstable]
description=deboostrap by tai
directory=/var/chroot/sid
root-users=ubuntu
users=ubuntu
type=directory
[UNRELEASED]
description=deboostrap by tai
directory=/var/chroot/sid
root-users=ubuntu
users=ubuntu
type=directory
EOT

sudo cp /tmp/poetry-build.conf /etc/schroot/chroot.d/poetry-build.conf

echo "you may need to edit /etc/schroot/chroot.d/poetry-build.conf like this (so we appended it for you):"

cat << EOF
[unstable]
description=deboostrap by tai
directory=/var/chroot/sid
root-users=ubuntu
users=ubuntu
type=directory
[UNRELEASED]
description=deboostrap by tai
directory=/var/chroot/sid
root-users=ubuntu
users=ubuntu
type=directory
EOF

echo
echo "you need to logout to make sbuild effective"
echo ""

