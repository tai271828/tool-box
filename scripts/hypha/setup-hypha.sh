#!/usr/bin/env bash
source config.sh

# edit to provide the superuser credentials
#(toolbox-kgJZ_AJs-py3.9) [^_^]─[~/work-my-projects/hypha-working/hypha] tai271828@syakaro: 220505-11:03:39 7 file 84Kb
#$ git diff
#diff --git a/hypha.php b/hypha.php
#index 94a1cc1..2f21d58 100644
#--- a/hypha.php
#+++ b/hypha.php
#@@ -6,8 +6,8 @@
#        //
#        // INSTALL
#        // 1. add your superuser login name and password here:
#-       $username = '';
#-       $password = '';
#+       $username = 'superuser1';
#+       $password = 'superuserpassword1';

# init hypha in apache
cp -r "${HYPHA_SRC}" "${HYPHA_SRC_INSTANCE}"
chmod +777 -R "${HYPHA_SRC_INSTANCE}"
sudo mkdir -p ${APACHE_DIR_ROOT_HYPHA}
sudo ln -s "${HYPHA_SRC_INSTANCE}" ${APACHE_DIR_ROOT_HYPHA}/${HYPHA_SRC_INSTANCE_NAME}

ls "${APACHE_DIR_ROOT}"
ls "${HYPHA_WORKING_DIR}"

echo
echo "deployed the instance"
echo "now invoke the first installation to launch the instance officially"
echo
# !!!!!!!!! base-url !!!!!!!! is a key!!!!!!!!!!!!!!!!
# fill in the info
echo "use for ${HYPHA_INSTANCE_URL} your base-url when filling installation configuration data"
# supseruser2 / superuserpassword2
echo
echo "(optional) browse ${HYPHA_INSTANCE_URL_PROVISION} to get your hypha instance installer"
echo "if you want to help distribute hypha"
echo
echo "visit ${HYPHA_INSTANCE_URL_INDEX} to launch the instance officially"
# user supseruser2's credential to login
