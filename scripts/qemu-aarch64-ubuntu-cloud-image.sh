#!/bin/bash
cloud_img_url="http://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-arm64.img"
macaddr=$(echo $HOSTNAME | md5sum | sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')
memory=8192

wget $cloud_img_url

# https://wiki.ubuntu.com/ARM64/QEMU
# static flash0 for the UEFI firmware
# dynamic flash1 for storing variables
#dd if=/dev/zero of=flash0.img bs=1M count=64
#dd if=/usr/share/qemu-efi/QEMU_EFI.fd of=flash0.img conv=notrunc
#dd if=/dev/zero of=flash1.img bs=1M count=64

cmd="sudo qemu-system-aarch64"
# since we run this script on aarch64 host
cmd="$cmd -enable-kvm -M virt"
cmd="$cmd -m $memory -cpu host"
# nogrphic so it will output to stdio a.k.a. your terminal
# note this differs from "none" and "ncursers"
cmd="$cmd -nographic"
# UEFI firmware
cmd="$cmd -pflash flash0.img -pflash flash1.img"
# put the content of drive "hd0" into the device virtio-blk-device
cmd="$cmd -drive if=none,file=focal-server-cloudimg-arm64.img,id=hd0"
cmd="$cmd -device virtio-blk-device,drive=hd0"
# networking
cmd="$cmd -netdev type=tap,id=net0 -device virtio-net-device,netdev=net0,mac=$macaddr"
