#!/bin/bash
#
#       backup-desktop.sh
#
#       backup-desktop.sh is based on backup_system_a_partition.sh
#
###################################################################
#
#	backup_system_a_partition.sh
#
#	Author:	thho
#	Descri:	Easy script to duplicate the system for ubuntu 10.04 under a directory
#	Date:	2010, 11, 23
#
#	Usage:	$ vi backup_system_a_partition.sh   // to specify the partitions
#		$ sudo su
#		$ ./backup_system_a_partition.sh > date.log 2>&1
#		Reboot with Live CD.
#               Note the Live CD should be the same version as your system
#               Rebuild /etc/fstab to mount on correct partitions
#		Rebuild grub2 with Live CD.
#		Rebuild grub2:
#			sudo su
#			mount /mnt/partition /dev/sdXY
#			grub-install --root-directory=/mnt/partition
#		Reboot the system. That is all!
#
#
#	Tips:
#		1.
#		Check which partition is which
#		$ ls -al /dev/disk/by-uuid/
#		2.
#		ubuntu 10.04 uses grub2, not a traditional grub
#
#
#
#


############# specify partitions ########################

targetdir=/
# specify to copy system files to which partition
#rootdir=/media/backup/
# the final slash is important
rootdir=/media/tai271828/2tb/

###########################################################

echo "Starting duplicating ... "
echo "Copying necessary files under $targetdir to $rootdir"
bkdate=`date +%F`
echo "Date is $bkdate"
rootdir=${rootdir}${bkdate}/
mkdir $rootdir
for target in boot lost+found mnt bin etc initrd.img lib opt root selinux usr vmlinuz boot initrd.img.old sbin srv var vmlinuz.old home
do
	object=$targetdir$target
	echo "copying $object to $rootdir"
	cp -a $object $rootdir
done
echo "Complete!"

