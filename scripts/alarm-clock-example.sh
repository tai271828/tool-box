#!/bin/bash
#
# Usage example:
# ./alarm-clock-example.sh -h
#

current_time=`date +%Y%m%d%H%M%S`

function usage() {
        echo "This is the usage"
        echo './alarm-clock-example.sh -c <command_script> -u <api_user> -k <api_key> -t <alarm_time>'
        echo 'alarm_time format:         YYYYmmddHHMMSS'
        echo 'alarm_time format example: 20131024172124'
}

function check_legal_alarm_time() {
        if [ $alarm_time \< $current_time ]; then
                echo "Alarm time is before current time"
                show_time_status
                exit
        fi
}

function show_time_status() {
        echo "Current is    $current_time"
        echo "Alarm time is $alarm_time"
}

function show_input_status() {
        echo $on_trigger_command
        show_time_status
}

function init_alarm() {
        on_trigger_command="$command_script -u $api_user -k $api_key -f"
        show_input_status

}

# fill the action you want to apply
function on_trigger_action() {
        $on_trigger_command
}

function check_current_time() {
        current_time=`date +%Y%m%d%H%M%S`
        if [ "$alarm_time" == "$current_time" ]; then
                echo "Action is triggered!!"
                on_trigger_action
        fi
}

while getopts "t:c:u:k:h" argv
do
        case $argv in
                t)
                        alarm_time=$OPTARG
                        check_legal_alarm_time
                        ;;
                c)
                        command_script=$OPTARG
                        ;;
                u)
                        api_user=$OPTARG
                        ;;
                k)
                        api_key=$OPTARG
                        ;;
                h)
                        usage
                        exit
                        ;;
        esac
done

init_alarm

while [ "$alarm_time" != "$current_time" ]
do
        #show_time_status
        check_current_time
        sleep 1
done
