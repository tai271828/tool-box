#!/usr/bin/env bash

set -e

source ./lxd-dhcp-vm-00-conf.sh


lxc network create ${lxd_network}
lxc network set ${lxd_network} ipv4.dhcp=false

lxc profile copy default ${lxd_profile}
lxc profile device remove ${lxd_profile} eth0
lxc profile device add ${lxd_profile} eth0 nic network=${lxd_network} name=eth0

lxc launch -p ${lxd_profile} ubuntu:22.04 ${lxd_vm_0} --vm
lxc launch -p ${lxd_profile} ubuntu:22.04 ${lxd_vm_1} --vm

lxc profile show ${lxd_profile}
lxc list
#lxc info ${lxd_vm_0}
#lxc info ${lxd_vm_1}
lxc network show ${lxd_network}

