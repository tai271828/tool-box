#!/usr/bin/env bash
# usage: ./01-schroot.sh debian arm64 bullseye

set -e

# debian or ubuntu
DISTRIBUTION=$1
# amd64 or arm64
ARCH=$2
# debian: sid, bullseye
# ubuntu: jammy, focal
RELEASE=$3


if [ "${DISTRIBUTION}" == "" ] || [ "${ARCH}" == "" ] || [ "${RELEASE}" == "" ]
then
  echo "Please explicitly specify the value of DISTRIBUTION ARCH RELEASE"
  exit 1
fi

source ./helpers/01-install-schroot.sh
source ./helpers/02-conf-schroot.sh
