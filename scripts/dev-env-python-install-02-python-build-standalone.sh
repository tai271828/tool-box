#!/usr/bin/env bash
#
# Get ready to use python-build pluging of pyenv
#

# install python-build as a standalone script

pushd /tmp

git clone https://github.com/pyenv/pyenv.git
cd pyenv
PREFIX="${HOME}/.local/" ./plugins/python-build/install.sh

echo
echo "Use python-build as a standalone application. Please note you can build python with, for example:"
echo " python-build 3.6.5 ~/.local/pythons/3.6"


popd
