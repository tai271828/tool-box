#!/bin/bash
#
# usage:
#     ./cmd.sh dict-spelling.txt > dict-spelling-sanitized.txt
#
#     mv dict-sanitized.txt dict-spelling.txt
#     tr '\n' ' ' < dict-spelling.txt
#     and copy-paste back to atom settings --> Packages --> Linter Spell --> Plain Text Words
#
xargs -n 1 < $1 | sort | uniq
