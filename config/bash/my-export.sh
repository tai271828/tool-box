#!/bin/bash
export MYLOCALINSTALLATION=$HOME/my-local-installation
#export SCSRC=$HOME/work-my-projects/solvcon/solvcon.installed
#export PYTHONPATH=$SCSRC:$PYTHONPATH
#export PATH="$PATH:$MYTOOLBOX/script/git-plugins"
# local installation pycharm
#export PATH="$MYLOCALINSTALLATION/pycharm-community-2016.1.3.installed/bin:$PATH"
# minikube
#export PATH="$HOME/work-my-projects/minikube:$PATH"

# miniconda
export MINICONDA_BASE="${HOME}/my-local-installation/miniconda3/base"

# where my zim notebooks are to get var like MYZIMNB
# note: MYZIMMN now also includes the notebooks or journal in the other formats like md from obsidian
if [ `hostname` == 'syakaro' ] || [ `hostname` == 'maibalai' ]; then
    source $HOME/Dropbox/tai271828-shared-config/zim-notebooks.sh
fi

# for local bin like pipenv
export PATH=$HOME/.local/bin:$PATH

# umlimited history, a number less than 0 means unlimited
# do not use this setup to aovoid to cause no history record
if [ `hostname` != 'yantok' ]; then
    export HISTSIZE=-1
    export HISTFILESIZE=-1
    export HISTFILE=${HOME}/.bash_eternal_history
    export HISTCONTROL=erasedups
fi

# enable calibre book management and viewer dark mode
export CALIBRE_USE_DARK_PALETTE=YES

export EDITOR='vi'
export VISUAL='vi'
