#!/bin/bash
# use the following bash function with "export -f <function name>" when issuing
# along with "xargs" piping
#
# KNOWN ISSUE:
# 1. if there is single quote in the commit message, it will be
# stripped and may not find the expacted commit for you.
#     WORKAROUND:
#     this is caused by the behavior of "xargs -a" to remove single quotes.
#     Apply slash before single quote in the commit file to keep single quotes.
# 2. only shows the latest commit. If there is any similar commit messages, they will be ignored.
#


# fundamental functions - helpers
get_git_tag_by_hash() {
  local tags_pattern_to_list
  local commit_hash
  local tags_pattern_to_show

  tags_pattern_to_list=$1
  commit_hash=$2
  tags_pattern_to_show=$3

  tag=$(git tag --list "${tags_pattern_to_list}" --contains "${commit_hash}" | \
        grep -E "${tags_pattern_to_show}")

  echo "${tag}"
}


get_git_tag_by_hash_help() {
  local tags_pattern_to_list
  local commit_hash
  local tags_pattern_to_show

  tags_pattern_to_list=$1
  commit_hash=$2
  tags_pattern_to_show=$3

  func_help_name=${FUNCNAME[0]}
  cmd_name=${func_help_name%%_help}
  echo "${cmd_name} tags_pattern_to_list commit_hash tags_pattern_to_show"
  echo "Example:"
  echo "${cmd_name} 'v[[:digit:]]*' bedd04e4aa1434d2f0f038e15bb6c48ac36876e1 '^v[[:digit:]]+\.[[:digit:]]+$'"

}


# fundamental functions - main commands
ggrep() {
  local repo_slash_branch
  local commit_msg_to_find

  repo_slash_branch=$1
  commit_msg_to_find=$2

  commits_all=$(git log "${repo_slash_branch}" --abbrev-commit --pretty=oneline \
              --grep "${commit_msg_to_find}")
  echo "${commits_all}"
}


ggrep_one_hash() {
  local repo_slash_branch
  local commit_msg_to_find

  repo_slash_branch=$1
  commit_msg_to_find=$2

  commit_hash=$(git log "${repo_slash_branch}" --abbrev-commit --pretty=oneline \
              --grep "${commit_msg_to_find}" -n 1 -i | cut -d' ' -f1)

  echo "${commit_hash}"
}


ggrep_tags() {
  local repo_slash_branch
  local commit_msg_to_find
  local tags_pattern_to_list
  local tags_pattern_to_show
  local commit_hash
  local found
  local not_found

  repo_slash_branch=$1
  commit_msg_to_find=$2
  tags_pattern_to_list=$3
  tags_pattern_to_show=$4

  commit_hash=$(ggrep_one_hash "${repo_slash_branch}" "${commit_msg_to_find}")
  if [ -n "$commit_hash" ]; then
    tag=$(get_git_tag_by_hash "${tags_pattern_to_list}" ${commit_hash} "${tags_pattern_to_show}")

    found="${tag} ${commit_hash} ${commit_msg_to_find}"
    echo ${found}
  else
    not_found="${commit_hash} ${commit_msg_to_find}"
    echo "Not found: ${not_found}"
  fi

}


ggrep_tags_help() {
  local repo_slash_branch
  local commit_msg_to_find
  local tags_pattern_to_list
  local tags_pattern_to_show

  repo_slash_branch=$1
  commit_msg_to_find=$2
  tags_pattern_to_list=$3
  tags_pattern_to_show=$4

  func_help_name=${FUNCNAME[0]}
  cmd_name=${func_help_name%%_help}
  echo "you may want to use `type bash-function-name`"
  echo
  echo "${cmd_name} repo_slash_branch commit_msg_to_find tags_pattern_to_list tags_pattern_to_show"
  echo "Example:"
  echo "${cmd_name} upstream/master foo-bar 'v[[:digit:]]*' '^v[[:digit:]]+\.[[:digit:]]+$'"

}


ggrep_tags_from_commits_file() {
  local file_commit_msgs
  local repo_slash_branch
  local tags_pattern_to_list
  local tags_pattern_to_show

  file_commit_msgs=$1
  repo_slash_branch=$2
  tags_pattern_to_list=$3
  tags_pattern_to_show=$4

  # DUMMY to pass arguments in expected order
  # try this and you will know:
  #     bash -c "echo $0" foobar bar
  #     bash -c "echo $1" foobar bar
  #     bash -c "echo $@" foobar bar
  xargs -a "${file_commit_msgs}" -I '{}' bash -c 'ggrep_tags "$@"' DUMMY \
        ${repo_slash_branch} '{}' ${tags_pattern_to_list} ${tags_pattern_to_show}
}


# applications
ggrep_tags_origin_master() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "origin/master" "${commit_msg_to_find}" \
             'v[[:digit:]]*' 'v[[:digit:]]*'
}

ggrep_tags_origin_master_noRC() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "origin/master" "${commit_msg_to_find}" \
             'v[[:digit:]]*' '^v[[:digit:]]+\.[[:digit:]]+$'
}


ggrep_tags_origin_hwe-5-11() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "origin/hwe-5.11" "${commit_msg_to_find}" \
             'Ubuntu-hwe-5.11-[[:digit:]]*' '^Ubuntu-hwe-*+\_+*'
}


ggrep_tags_upstream_master() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "upstream/master" "${commit_msg_to_find}" \
             'v[[:digit:]]*' 'v[[:digit:]]*'
}

ggrep_tags_upstream_master_noRC() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "upstream/master" "${commit_msg_to_find}" \
             'v[[:digit:]]*' '^v[[:digit:]]+\.[[:digit:]]+$'
}


ggrep_tags_lp_master() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/master" "${commit_msg_to_find}" \
	     'Ubuntu-[[:digit:]]*' '^Ubuntu-*+\-+*'
}


ggrep_tags_lp_master-next() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/master-next" "${commit_msg_to_find}" \
	     'Ubuntu-[[:digit:]]*' '^Ubuntu-*+\-+*'
}


ggrep_tags_lp_hwe() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/hwe" "${commit_msg_to_find}" \
	     'Ubuntu-hwe-[[:digit:]]*' '^Ubuntu-hwe-*+\_+*'
}


ggrep_tags_lp_hwe-edge() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/hwe-edge" "${commit_msg_to_find}" \
	     'Ubuntu-hwe-edge-[[:digit:]]*' '^Ubuntu-hwe-edge-*+\_+*'
}


ggrep_tags_lp_hwe-5-4() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/hwe-5.4" "${commit_msg_to_find}" \
             'Ubuntu-hwe-5.4-[[:digit:]]*' '^Ubuntu-hwe-*+\_+*'
}


ggrep_tags_lp_hwe-5-8() {
  local commit_msg_to_find

  commit_msg_to_find=$1

  ggrep_tags "lp/hwe-5.8" "${commit_msg_to_find}" \
             'Ubuntu-hwe-5.8-[[:digit:]]*' '^Ubuntu-hwe-*+\_+*'
}


ggrep_tags_origin_master_from_commits_file() {
  ggrep_tags_from_commits_file $1 "origin/master" 'v[[:digit:]]*' 'v[[:digit:]]*'
}


ggrep_tags_origin_master_noRC_from_commits_file() {
  ggrep_tags_from_commits_file $1 "origin/master" 'v[[:digit:]]*' '^v[[:digit:]]+\.[[:digit:]]+$'
}


ggrep_tags_upstream_master_from_commits_file() {
  ggrep_tags_from_commits_file $1 "upstream/master" 'v[[:digit:]]*' 'v[[:digit:]]*'
}


ggrep_tags_upstream_master_noRC_from_commits_file() {
  ggrep_tags_from_commits_file $1 "upstream/master" 'v[[:digit:]]*' '^v[[:digit:]]+\.[[:digit:]]+$'
}


ggrep_tags_lp_master_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/master" 'Ubuntu-[[:digit:]]*' '^Ubuntu-*+\-+*'
}


ggrep_tags_lp_master-next_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/master-next" 'Ubuntu-[[:digit:]]*' '^Ubuntu-*+\-+*'
}


ggrep_tags_lp_hwe_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/hwe" 'Ubuntu-hwe-[[:digit:]]*' '^Ubuntu-hwe-*+\-+*'
}


ggrep_tags_lp_hwe-edge_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/hwe-edge" 'Ubuntu-hwe-edge-[[:digit:]]*' '^Ubuntu-hwe-edge-*+\-+*'
}


ggrep_tags_lp_hwe-5-4_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/hwe-5.4" 'Ubuntu-hwe-5.8-[[:digit:]]*' '^Ubuntu-hwe-*+\-+*'
}


ggrep_tags_lp_hwe-5-8_from_commits_file() {
  ggrep_tags_from_commits_file $1 "lp/hwe-5.8" 'Ubuntu-hwe-5.8-[[:digit:]]*' '^Ubuntu-hwe-*+\-+*'
}


# to use these functions directly when invoking xargs
# example: xargs -a lp1882328-commits.txt -I '{}' bash -c 'ggrep_lp_hwe_5_8_tags "$@"' DUMMY '{}'
export -f get_git_tag_by_hash
export -f get_git_tag_by_hash_help
export -f ggrep
export -f ggrep_one_hash
export -f ggrep_tags
export -f ggrep_tags_help

export -f ggrep_tags_origin_master
export -f ggrep_tags_origin_master_from_commits_file
export -f ggrep_tags_origin_master_noRC
export -f ggrep_tags_origin_master_noRC_from_commits_file
export -f ggrep_tags_origin_hwe-5-11

export -f ggrep_tags_upstream_master
export -f ggrep_tags_upstream_master_from_commits_file
export -f ggrep_tags_upstream_master_noRC
export -f ggrep_tags_upstream_master_noRC_from_commits_file

export -f ggrep_tags_lp_master
export -f ggrep_tags_lp_master_from_commits_file
export -f ggrep_tags_lp_master-next
export -f ggrep_tags_lp_master-next_from_commits_file

export -f ggrep_tags_lp_hwe
export -f ggrep_tags_lp_hwe_from_commits_file
export -f ggrep_tags_lp_hwe-edge
export -f ggrep_tags_lp_hwe-edge_from_commits_file
export -f ggrep_tags_lp_hwe-5-4
export -f ggrep_tags_lp_hwe-5-4_from_commits_file
export -f ggrep_tags_lp_hwe-5-8
export -f ggrep_tags_lp_hwe-5-8_from_commits_file
