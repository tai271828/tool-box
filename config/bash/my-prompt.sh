#!/bin/bash

########## shell prompt ############################
# this link has many interesting prompt examples
# http://www.maketecheasier.com/8-useful-and-interesting-bash-prompts/2009/09/04
# \[\e[37m\]    \[\e[0m\] <------- dark color braket
# \[\033[32m\]  \[\033[0m\] <---- highlight color braket
# \n
# \w <-------- working path
# [0;31m] dark white, [1;37m] light white
# please use \ in front of ; in if of `` to avoid the syntax key word
#
# deb_c_prefix: append chroot prefix when being in the chroot env
#   [^_^]─[~] tai271828@syakaro: 55 file 254Mb
#   $ schroot -c unstable
#   (unstable)[^_^]─[~] tai271828@syakaro: 55 file 254Mb
#   $
#
deb_c_prefix=${debian_chroot:+($debian_chroot)}
PS1="\`if [ \$? = 0 ]; then echo \${deb_c_prefix}\[\e[1\;37m\][\[\e[0m\]\[\e[0\;33m\]^_^\[\e[0m\]\[\e[1\;37m\]]\[\e[0m\]; else echo \${deb_c_prefix}\[\e[37m\][\[\e[0m\]\[\033[1\;31m\]O_o\[\e[0m\]\[\e[37m\]]\[\e[0m\]; fi\`\[\033[1;37m\]\342\224\200[\[\e[0m\]\[\e[32m\]\w\[\e[0m\]\[\033[1;37m\]]\[\e[0m\] \[\e[35m\]\u@\h:\[\e[0m\] \D{%y%m%d-%H:%M:%S} \[\e[36m\]\$(/bin/ls -1 | /usr/bin/wc -l | /bin/sed 's: ::g') file \[\e[33m\]\$(/bin/ls -lah | /bin/grep -m 1 total | /bin/sed 's/total //')b\[\e[0m\]\[\e[0m\]\n\$ "

