#!/bin/bash
# hook-all.sh
# hook this script in 
#   ~/.bash_aliases for Ubuntu or
#   ~/.bashrc in general
# to make my bash environment configuration effective
#
# ~/.bash_aliases
# source $HOME/toolbox/config/bash/hook-all.sh
#
#####################################################
################# change me ################
export MYTOOLBOX="$HOME/toolbox"

export PATH=${MYTOOLBOX}/bin:${PATH}

######## Optional ##########
export VOCABULARY_DB="$HOME/Dropbox/Apps/Biscuit/160104to-chosen160805.txt"

#####################################################
MYBASH_HOOKUP=$MYTOOLBOX/config/bash
source $MYBASH_HOOKUP/my-export.sh
source $MYBASH_HOOKUP/my-work-config.sh
source $MYBASH_HOOKUP/my-aliases.sh
source $MYBASH_HOOKUP/my-python-dev-env.sh
source $MYBASH_HOOKUP/my-function.sh
source $MYBASH_HOOKUP/my-prompt.sh
source $MYBASH_HOOKUP/my-deb-env.sh
source $MYBASH_HOOKUP/my-config.sh
source $MYBASH_HOOKUP/my-kernel-tree-tools.sh
# something should be sourced at the bottom
source $MYBASH_HOOKUP/my-source-bottom.sh
