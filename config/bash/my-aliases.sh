#!/bin/bash
#
# man bash: "aliases are superseded by shell functions"
#

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias vi='vim'
alias gvi='gvim'

alias thho-pb='pbuild -p offical-precise-amd64'

# ssh sliently
alias sshs="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
alias sshsp="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o PreferredAuthentications=password"
alias scps="scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

# hook up my tools
alias md5-verify="$HOME/toolbox/script/md5-verify.sh"

# git
alias gc="git config --list"
alias gd="git diff"
alias gs="git status"
alias gg="git log --graph"
alias gb="git branch"
alias gcs="git commit -S"
alias gcss="git commit -s -S"

# use anaconda, ipython 2.0 instead of system ipython
alias anacon="export PATH=$HOME/anaconda/bin:$PATH"
# use miniconda
alias minicon="export PATH=$MYLOCALINSTALLATION/miniconda/general/bin:$PATH"
alias minicon3="export PATH=$MYLOCALINSTALLATION/miniconda3/general/bin:$PATH"

# SOLVCON env
alias solvconenv-py2="export PATH=$MYLOCALINSTALLATION/miniconda2/solvcon-dev/bin:$PATH; source $HOME/work-my-projects/solvcon/solvcon/build/env/start"
alias solvconenv="export PATH=$MYLOCALINSTALLATION/miniconda3/solvcon-dev/bin:$PATH; source $HOME/work-my-projects/solvcon/solvcon/build/env/start"
alias gas="cd $HOME/work-my-projects/solvcon/solvcon/sandbox/gas/tube"

# the other development
alias cc-sru-review-mc="export PATH=$MYLOCALINSTALLATION/miniconda3/cc-sru-review/bin:$PATH; source activate $MYLOCALINSTALLATION/miniconda3/cc-sru-review"
alias cc-sru-review-code="cd $HOME/work/cc-lab/cc-sru-review"
alias cc="cd $HOME/work/cc-lab"

# connection of hosts
alias lab533="sshs tai271828@${LAB533}"
# termont lab build server
alias bs="sshs ubuntu@${TREMONT_BS}"

# binding zim and auto-generated files
# you need to edit
# /usr/share/applications/zim.desktop
# Exec=/home/tai271828/toolbox/script/custimize-zim

# listen to online radio
# you may need to install mplayer first
#
# the location was gotten from the source code of npr website
#
alias online-radio-npr-24="mplayer http://nprdmp.ic.llnwd.net/stream/nprdmp_live01_mp3"

# poetry
pyenv_python_versions=${HOME}/.pyenv/versions
poetry_python_version=3.8.3
poetry_init_command="poetry init --python ${poetry_python_version} -n; poetry env use ${pyenv_python_versions}/bin/python; poetry install"
function poetry_init_this_project_with_python() {
    local poetry_python_version=$1

    poetry init --python ${poetry_python_version} -n
    poetry env use ${pyenv_python_versions}/${poetry_python_version}/bin/python
    poetry install
}

function poetry_init_this_project_with_python_3_8_3() {
    poetry_init_this_project_with_python 3.8.3
}

function poetry_init_this_project_with_python_3_9_1() {
    poetry_init_this_project_with_python 3.9.1
}

function poetry_init_this_project_with_python_3_9_13() {
    poetry_init_this_project_with_python 3.9.13
}

if [ `hostname` == 'nanhu' ]; then
    alias google-chrome-stable='google-chrome-stable --force-device-scale-factor';
fi

lxcsh() { lxc exec "$1" -- sudo --login --user ubuntu; }
